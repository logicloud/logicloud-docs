# Memberstack

<img src="https://assets-global.website-files.com/5bbfaf3252489b4c484ba9b9/653771c3e0d0179dc92b4c43_full%20logo%20-%20black.svg" alt="Gather" width="300px" style="width: 300px;"/>

## About Memberstack
Memberstack manages Authentication and Payments for over 5.5 million people & 2400+ companies. Memberstack is a SaaS product that helps Webflow developers add secure user accounts and payments in a fraction of the time. Memberstack enables businesses to launch custom software faster, reduce complexity, and save tons of money without compromising on design or functionality.

Website: [Memberstack](https://www.memberstack.com/)

---

## The Challenge
As is the case with most startups, Memberstack initially configured their AWS environment and account structure for their initial requirements which they had outgrown. Memberstack was gearing up for a SOC 2 audit, which required tighter controls around their AWS accounts; such as, dedicated AWS accounts for each environment, a better AWS authentication strategy as well as implementing AWS best practices for their infrastructure.

---

## Solution
LogiCloud assisted Memberstack in the following ways:
- Moving all resources that were currently managed in the management account to separate accounts for staging and production.
- Implementing a multi-AZ configuration of the staging and production RDS clusters as well as making both clusters private.
- Implement a secure solution for logging in to AWS using SSO.

The solution was built using Terraform which allows Memberstack to maintain and manage their infrastructure using infrastructure as code.

---

## Partnership
Memberstack has partnered with LogiCloud as their preferred AWS partner, which has led to additional collaboration implementing AWS best practices following the AWS Well Architected Framework.
